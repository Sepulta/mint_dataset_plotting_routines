import numpy as np
import pandas as pd
from functions import readout_datafile_and_return_dataframes, plot_data_2d, create_plotting_data, plot_with_unstructured_data
import matplotlib.pyplot as plt



example_grid_CHeB = 'example_data/example_CHeB_grid/grid_Z0p02_degen_v4.dat'
dataframe_dict, non_chebychev_column_names_dict  = readout_datafile_and_return_dataframes(example_grid_CHeB)


# Create a combined dataframe
combined_df = pd.DataFrame()
for df_key, df in dataframe_dict.items():
    df['AGE'] = df['AGE']-df['AGE'].iloc[0]

    combined_df = pd.concat([combined_df, df], ignore_index=True)

# Get the dataframe
x_param = 'HELIUM_CORE_MASS'
y_param = 'CENTRAL_HELIUM'
z_param = 'AGE'

query = 'MASS <= 2'.format(combined_df['MASS'].unique()[1])

combined_df = combined_df.query(query)[[x_param, y_param, z_param]]

#
y_grid = np.linspace(combined_df[y_param].min(), combined_df[y_param].max(), 1000)

# Set up z-grid with nans
x_grid = combined_df[x_param].unique()
z_grid = np.zeros((x_grid.shape[0], y_grid.shape[0]))
z_grid[:,:] = np.nan

# html = combined_df.to_html()
# with open("html_table.html", 'w') as f:
#     f.write(html)

# quit()
from scipy import interpolate

for df_i, df in enumerate([combined_df.query("{} == {}".format(x_param, val)) for val in sorted(x_grid)]):

    #
    if not df.empty:

        # Just fill in the same values
        if len(df.index) == 1:
            pass

        #
        else:
            # Find current min and max
            current_min, current_max = df[y_param].min(), df[y_param].max()

            # Set up the interpolator
            interp = interpolate.interp1d(df[y_param], df[z_param])

            # Get all the y_grid values that are within the current value range
            indices_current_grid = np.where(np.logical_and(y_grid>=current_min, y_grid<=current_max))
            current_grid = y_grid[indices_current_grid]
            current_values = interp(current_grid)

            #
            z_grid[df_i][indices_current_grid] = current_values
            print(df[x_param].iloc[0])
            # plt.scatter([df[x_param].iloc[0]]*len(current_grid), current_grid, c=current_values)
plt.title(query)
plt.scatter(combined_df[x_param], combined_df[y_param], c=np.log10(combined_df[z_param]))
plt.xscale('log')
plt.show()

# plot the data
plot_data_2d(
    x_grid,
    y_grid,
    z_grid,
    x_scale='linear',
    y_scale='linear',
    z_scale='log',
    x_label='',
    y_label='',
    z_label='',
    plot_settings={'show_plot': True}
)
