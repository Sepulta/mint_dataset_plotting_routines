import astropy.units as u
import numpy as np

from functions import readout_datafile_and_return_dataframes, plot_data_2d, create_plotting_data, create_x_y_grids, fill_z_grid_by_slices, plot_with_unstructured_data

import matplotlib.pyplot as plt
from matplotlib import colors
import matplotlib as mpl
from matplotlib.tri import Triangulation

from plot_utility_functions import add_plot_info, show_and_save_plot, load_mpl_rc, linestyles
load_mpl_rc()

#
input_file = 'example_data/example_HG_grid/grid_Z1.42e-02_withE.dat'
# input_file = 'example_data/example_MS_grid/example_new.dat'

dataframe_dict, non_chebychev_column_names_dict  = readout_datafile_and_return_dataframes(input_file)






#
x_param = 'MASS'
y_param = 'BURNING_SHELL_THICKNESS'
z_param = 'AGE'

x_scale = 'log'
y_scale = 'log'
z_scale = 'log'

plot_with_unstructured_data(
    dataframe_dict,
    x_param,
    y_param,
    z_param,
    x_scale,
    y_scale,
    z_scale,
    y_resolution=1000,
    contourlevel_dict=contourlevel_dict,
    label_dict=label_dict,
    plot_settings={'runname': 'LUMINOSITY', 'show_plot': True, 'output_name': 'test.pdf'}
)