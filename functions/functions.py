"""
Functions to read out the MINT data sets
"""

import copy
import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import colors
import matplotlib as mpl

from scipy import interpolate

import warnings

from functions.plot_utility_functions import (
    add_plot_info,
    show_and_save_plot,
    load_mpl_rc,
    linestyles
)

load_mpl_rc()
warnings.simplefilter("ignore", UserWarning)

def read_header_and_data(filename):
    """
    Function to read out the datafile, and find header lines and datalines
    """

    #
    header_lines = []
    data_lines = []

    finished_headers = 0

    # read in data
    with open(filename, 'r') as f:
        for line in f:
            if not finished_headers:
                header_lines.append(line.strip())
            else:
                data_lines.append(line)

            if line.startswith("# END HEADER"):
                finished_headers = 1

    #
    return header_lines, data_lines

def get_indices_column_descriptors(header_lines):
    """
    Function to get the first and last index of the column descriptors
    """

    # Find indices of column descriptors:
    start_index_column_descriptors = header_lines.index("# START MINT COLUMN DESCRIPTORS")
    end_index_column_descriptors = header_lines.index("# END MINT COLUMN DESCRIPTORS")

    return start_index_column_descriptors, end_index_column_descriptors

def get_indices_and_column_names(header_lines):
    """
    Function to get the indices and column names for the header
    """

    #
    start_index_column_descriptors, end_index_column_descriptors = get_indices_column_descriptors(header_lines)

    # Get the column name information
    column_names = copy.copy(header_lines[start_index_column_descriptors+2:end_index_column_descriptors-1])

    #
    return start_index_column_descriptors, end_index_column_descriptors, column_names

def get_data_columns(data_lines, max_value_non_chebychev):
    """
    Function to read out the data
    """

    data_non_chebychev = []
    data_chebychev = []

    # Properly read out the data now:
    for data_line in data_lines:
        split = data_line.split()

        data_non_chebychev.append([float(el) for el in split[:max_value_non_chebychev]])
        data_chebychev.append(split[max_value_non_chebychev:])

    return data_non_chebychev, data_chebychev

def get_non_chebychev_header_info(column_names):
    """
    Function to read out the non-chebychev (i.e. non-structural) information

    TODO: change the non-chebychev to some other name
    """

    #
    column_names_dict = {}
    column_names_list = []

    #
    max_value_non_chebychev = 0 # counter for the maximum row
    for column_name in column_names:
        if not column_name.startswith("# CHEBYSHEV"):
            split = column_name[2:].split()
            name = split[0]
            value = int(split[1])

            column_names_dict[name] = value
            column_names_list.append(name)

            if max_value_non_chebychev < value:
                max_value_non_chebychev = value
                value = max_value_non_chebychev

    return column_names_dict, column_names_list, max_value_non_chebychev

def select_data_by_mass(data_non_chebychev, non_chebychev_column_names_dict):
    """
    Function to select the data by mass
    """

    # Set up a dictionary to hold the info for each mass
    non_chebychev_data_per_mass_dict = {}

    for line in data_non_chebychev:
        current_mass = line[non_chebychev_column_names_dict['MASS']-1]

        if not current_mass in non_chebychev_data_per_mass_dict:
            non_chebychev_data_per_mass_dict[current_mass] = []
        non_chebychev_data_per_mass_dict[current_mass].append(line)

    return non_chebychev_data_per_mass_dict

# Turn the info into dataframes
def turn_data_into_dataframes(non_chebychev_data_per_mass_dict, non_chebychev_column_names_list):
    """
    Function to turn the data per mass dict into a dict containing dataframes
    """

    dataframe_dict = {}

    for mass in sorted(non_chebychev_data_per_mass_dict.keys()):
        dataframe_dict[mass] = pd.DataFrame(non_chebychev_data_per_mass_dict[mass], columns=non_chebychev_column_names_list)

    return dataframe_dict

def readout_datafile_and_return_dataframes(input_file):
    """
    Function that handles the whole readout process and returns dataframes
    """

    # Read out the file and save the header and data lines
    header_lines, data_lines = read_header_and_data(input_file)

    # Get the indices for the index/header columns and the column names
    start_index_column_descriptors, end_index_column_descriptors, column_names = get_indices_and_column_names(header_lines)

    # Get index dict and name list for the non-chebychev columns
    non_chebychev_column_names_dict, non_chebychev_column_names_list, max_value_non_chebychev = get_non_chebychev_header_info(column_names)

    # Get the data of the non-chebychev and the chebychev columns
    data_non_chebychev, data_chebychev = get_data_columns(data_lines, max_value_non_chebychev)

    # Read out the data and store it per mass
    non_chebychev_data_per_mass_dict = select_data_by_mass(data_non_chebychev, non_chebychev_column_names_dict)

    # Turn the data into dataframes
    dataframe_dict = turn_data_into_dataframes(non_chebychev_data_per_mass_dict, non_chebychev_column_names_list)

    return dataframe_dict, non_chebychev_column_names_dict

def create_plotting_data(dataframe_dict, x_column, y_column, z_column):
    """
    Function to create the data (x, y, z arrays) of the columns we want to plot. Will raise error when trying to plot something that is not there
    """

    #
    x_arrays = []
    y_arrays = []
    z_arrays = []

    #
    for dataframe_key in dataframe_dict:
        x_arrays.append(dataframe_dict[dataframe_key][x_column].to_numpy())
        y_arrays.append(dataframe_dict[dataframe_key][y_column].to_numpy())
        z_arrays.append(dataframe_dict[dataframe_key][z_column].to_numpy())

    #
    x_arrays = np.array(x_arrays)
    y_arrays = np.array(y_arrays)
    z_arrays = np.array(z_arrays)

    return x_arrays, y_arrays, z_arrays


def plot_data_2d(x_arrays, y_arrays, z_arrays, x_scale='log', y_scale='log', z_scale='log', x_label='', y_label='', z_label='', contourlevels=None, linestyles_contourlevels=None, plot_settings=None):
    """
    Function to handle the plotting of the 2-d data, including adding a colorbar and contourlines
    """

    if plot_settings is None:
        plot_settings = {}

    #
    filtered_z_arrays = z_arrays[z_arrays!=0]
    filtered_z_arrays = filtered_z_arrays[filtered_z_arrays>-1e90]

    #####
    # Set up the figure
    fig = plt.figure(
        figsize=(20, 20)
    )

    # It could be that the filtered z_array is entirely empty. In that case, dont do anything anymore and just save the figure and leave
    if not filtered_z_arrays.size == 0:
        min_plot_value = filtered_z_arrays.min()
        max_plot_value = filtered_z_arrays.max()

        # The z scale: needs to be decided here so we can use it in the plot
        if z_scale=='linear':
            norm = colors.Normalize(
                vmin=min_plot_value,
                vmax=max_plot_value
            )
        elif z_scale=='log':
            norm=colors.LogNorm(
                vmin=min_plot_value,
                vmax=max_plot_value
            )
        elif z_scale=='symlog':
            smallest_scale = np.abs(filtered_z_arrays)[np.abs(filtered_z_arrays)>0].min()

            #
            norm=colors.SymLogNorm(
                linthresh=smallest_scale,
                linscale=2,
                vmin=min_plot_value, vmax=max_plot_value,
                base=10
            )
        else:
            msg = 'zscale option {} not available. Abort'.format(z_scale)
            print(msg)
            raise ValueError(msg)

        #
        gs = fig.add_gridspec(nrows=1, ncols=11)

        ax = fig.add_subplot(gs[:, :9])

        ax_cb = fig.add_subplot(gs[:, 10])

        #
        _ = ax.pcolormesh(
            x_arrays,
            y_arrays,
            z_arrays,
            norm=norm,
            shading='auto',
            antialiased=True,
            rasterized=True,
        )

        # make colorbar
        cbar = mpl.colorbar.ColorbarBase(
            ax_cb,
            cmap=mpl.cm.viridis,
            norm=norm,
        )

        # Set contourlevens and lines on the colorbar
        if (not contourlevels is None) and (not linestyles_contourlevels is None) and (len(contourlevels) <= len(linestyles_contourlevels)):
            CS_mt_region_1 = ax.contour(
                x_arrays,
                y_arrays,
                z_arrays,
                levels=contourlevels,
                colors='k',
                linestyles=linestyles_contourlevels,
            )

            # Set lines on the colorbar
            for contour_i, _ in enumerate(contourlevels):
                cbar.ax.plot([cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]], [contourlevels[contour_i]]*2, 'black', linestyle=linestyles_contourlevels[contour_i])

        ####
        # Make up
        ax.set_xlabel(x_label)
        ax.set_ylabel(y_label)
        cbar.ax.set_ylabel(z_label)

        #
        ax.set_ylim([y_arrays.min(), y_arrays.max()])
        ax.set_xlim([x_arrays.min(), x_arrays.max()])

        #
        ax.set_xscale(x_scale)
        ax.set_yscale(y_scale)

        #
        fig.tight_layout()
    else:
        print("Found empty dataset")

    #
    if plot_settings.get('add_plot_info', True):
        # Call plot info (time generated etc)
        fig = add_plot_info(fig, plot_settings)

    #######################
    # Save and finish
    show_and_save_plot(plot_settings)


def get_xvalues_and_global_limits(dataframe_dict, y_param, y_scale, query=None):
    """
    Function to get the global min and max value
    """

    #
    global_min = 1e90
    global_max = -1e90

    # Find x-valus and global_min and global_max
    x_values = []
    for df_key, df in dataframe_dict.items():
        x_values.append(float(df_key))

        # Perform global query;
        if not query is None:
            df = df.query(query)

        if not df.empty:
            yparams = df[y_param]
            if y_scale == 'log':
                yparams = df.query('{}>0'.format(y_param))[y_param]

            # find global min and max:
            global_min = np.min([global_min, yparams.min()])
            global_max = np.max([global_max, yparams.max()])

    if global_min==np.nan or global_max==np.nan:
        raise ValueError("The global min or max is nan: global_min: {} global_max: {}", global_min, global_max)

    return x_values, global_min, global_max

def create_x_y_grids(dataframe_dict, y_param, y_resolution, y_scale, query=None):
    """
    Function to create the x and y grids
    """

    #
    x_values, global_min, global_max = get_xvalues_and_global_limits(dataframe_dict, y_param, y_scale, query=query)

    # Create x and y arrays
    y_grid = np.linspace(global_min, global_max, y_resolution)
    if y_scale == 'log':
        y_grid = 10**np.linspace(np.log10(global_min), np.log10(global_max), y_resolution)
    x_grid = np.array(x_values)

    return x_grid, y_grid

def fill_z_grid_by_slices(y_grid, z_grid, dataframe_dict, y_param, z_param, query=None):
    """
    Function to fill the z-grid by setting up interpolation and filling the part of the z-grid that is spanned by the local min and max with interpolated data
    """

    #
    for df_i, (df_key, df) in enumerate(dataframe_dict.items()):
        # Perform global query
        if not query is None:
            df = df.query(query)
        # Find current min and max
        current_min, current_max = df[y_param].min(), df[y_param].max()

        if not df.empty:
            # Set up the interpolator
            interp = interpolate.interp1d(df[y_param], df[z_param])

            # Get all the y_grid values that are within the current value range
            indices_current_grid = np.where(np.logical_and(y_grid>=current_min, y_grid<=current_max))
            current_grid = y_grid[indices_current_grid]
            current_values = interp(current_grid)

            #
            z_grid[df_i][indices_current_grid] = current_values

    #
    return z_grid

def plot_with_unstructured_data(dataframe_dict, x_param, y_param, z_param, x_scale, y_scale, z_scale, y_resolution=1000, contourlevel_dict={}, label_dict={}, plot_settings=None, query=None):
    """
    Main function to plot the unstructured data in a grid
    """

    #
    if plot_settings is None:
        plot_settings = {}

    # Get x_grid and y_grid
    x_grid, y_grid = create_x_y_grids(dataframe_dict, y_param, y_resolution, y_scale, query=query)

    # Set up z-grid with nans
    z_grid = np.zeros((x_grid.shape[0], y_grid.shape[0]))
    z_grid[:,:] = np.nan

    # Fill the z-grid, in the correct slices, with the interpolated data
    z_grid = fill_z_grid_by_slices(y_grid, z_grid, dataframe_dict, y_param, z_param, query=query)

    # plot the data
    plot_data_2d(
        x_grid,
        y_grid,
        z_grid.T,
        x_scale=x_scale,
        y_scale=y_scale,
        z_scale=z_scale,
        x_label=label_dict[x_param],
        y_label=label_dict[y_param],
        z_label=label_dict[z_param],
        contourlevels=contourlevel_dict.get(z_param, []),
        linestyles_contourlevels=linestyles,
        plot_settings=plot_settings
    )
