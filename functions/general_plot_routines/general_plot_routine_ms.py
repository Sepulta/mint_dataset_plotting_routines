"""
General plot routine for the MS grid
"""

import os

from PyPDF2 import PdfFileMerger

from functions.functions import readout_datafile_and_return_dataframes, plot_data_2d, create_plotting_data
from functions.plot_utility_functions import add_pdf_and_bookmark

# TODO: put this in a settings file or something
Z_SCALE_DICT = {
    'AGE': 'log',
    'CONVECTIVE_CORE_MASS': 'log',
    'CONVECTIVE_CORE_MASS_OVERSHOOT': 'log',
    'CONVECTIVE_CORE_RADIUS': 'log',
    'CONVECTIVE_CORE_RADIUS_OVERSHOOT': 'log',
    'CONVECTIVE_ENVELOPE_MASS': 'log',
    'CONVECTIVE_ENVELOPE_MASS_SIMPLIFIED': 'log',
    'CONVECTIVE_ENVELOPE_RADIUS': 'log',
    'CONVECTIVE_ENVELOPE_RADIUS_SIMPLIFIED': 'log',
    'FIRST_DERIVATIVE_CENTRAL_HYDROGEN': 'symlog',
    'K2': 'log',
    'LUMINOSITY': 'log',
    'MEAN_MOLECULAR_WEIGHT_AVERAGE': 'log',
    'MEAN_MOLECULAR_WEIGHT_CORE': 'log',
    'MOMENT_OF_INERTIA_FACTOR': 'log',
    'RADIUS': 'log',
    'SECOND_DERIVATIVE_CENTRAL_HYDROGEN': 'symlog',
    'TIDAL_E2': 'log',
    'TIDAL_E_FOR_LAMBDA': 'log',
    'TIMESCALE_DYNAMICAL': 'log',
    'TIMESCALE_KELVIN_HELMHOLTZ': 'log',
    'TIMESCALE_NUCLEAR': 'log',
}


def general_plot_routine(dataset_filename, output_pdfs_dir, combined_pdf_filename, show_plots=False):
    """
    Function to plot all the columns (non-structural) of a MINT dataset.

    Input:
        dataset_filename: path to the MINT dataset
    """

    # Make output dir if necessary:
    os.makedirs(output_pdfs_dir, exist_ok=True)

    # Get the dataframes and some info about the columns
    dataframe_dict, non_chebychev_column_names_dict  = readout_datafile_and_return_dataframes(dataset_filename)

    # Determine the columns we need to plot
    all_column_names = sorted(non_chebychev_column_names_dict.keys(), key=lambda x: x)
    xy_column_names = ['MASS', 'CENTRAL_HYDROGEN']
    plot_column_names = sorted(list(set(all_column_names)-set(xy_column_names)))

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_page_number = 0

    # Go over the columns
    for column_name in plot_column_names:
        output_name = os.path.join(output_pdfs_dir, '{}.pdf'.format(column_name))

        x_arrays, y_arrays, z_arrays = create_plotting_data(dataframe_dict, 'MASS', 'CENTRAL_HYDROGEN', column_name)
        plot_data_2d(
            x_arrays,
            y_arrays,
            z_arrays,
            z_scale=Z_SCALE_DICT[column_name],
            z_label=column_name,
            # contourlevels,
            # linestyles_contourlevels,
            plot_settings={'runname': column_name, 'show_plot': show_plots, 'output_name': output_name}
        )
        merger, pdf_page_number = add_pdf_and_bookmark(merger, output_name, pdf_page_number, bookmark_text="{}".format(column_name))

    # wrap up the pdf
    merger.write(combined_pdf_filename)
    merger.close()
    print("\t wrote combined pdf to {}".format(combined_pdf_filename))
