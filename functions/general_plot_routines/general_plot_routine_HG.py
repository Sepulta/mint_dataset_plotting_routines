"""
General plot routine for the MS grid
"""

import os

from PyPDF2 import PdfFileMerger

from functions.functions import readout_datafile_and_return_dataframes, plot_data_2d, create_plotting_data, plot_with_unstructured_data
from functions.plot_utility_functions import add_pdf_and_bookmark

Z_SCALE_DICT = {
    'AGE': 'log',
    'CONVECTIVE_CORE_MASS': 'log',
    'CONVECTIVE_CORE_MASS_OVERSHOOT': 'log',
    'CONVECTIVE_CORE_RADIUS': 'log',
    'CONVECTIVE_CORE_RADIUS_OVERSHOOT': 'log',
    'CONVECTIVE_ENVELOPE_MASS': 'log',
    'CONVECTIVE_ENVELOPE_MASS_SIMPLIFIED': 'log',
    'CONVECTIVE_ENVELOPE_RADIUS': 'log',
    'CONVECTIVE_ENVELOPE_RADIUS_SIMPLIFIED': 'log',
    'FIRST_DERIVATIVE_CENTRAL_HYDROGEN': 'symlog',
    'K2': 'log',
    'LUMINOSITY': 'log',
    'MEAN_MOLECULAR_WEIGHT_AVERAGE': 'log',
    'MEAN_MOLECULAR_WEIGHT_CORE': 'log',
    'MOMENT_OF_INERTIA_FACTOR': 'log',
    'RADIUS': 'log',
    'SECOND_DERIVATIVE_CENTRAL_HYDROGEN': 'symlog',
    'TIDAL_E2': 'log',
    'TIDAL_E_FOR_LAMBDA': 'log',
    'TIMESCALE_DYNAMICAL': 'log',
    'TIMESCALE_KELVIN_HELMHOLTZ': 'log',
    'TIMESCALE_NUCLEAR': 'log',

    'FIRST_DERIVATIVE_BURNING_SHELL_THICKNESS': 'symlog',
    'SECOND_DERIVATIVE_BURNING_SHELL_THICKNESS': 'symlog',
    'BURNING_SHELL_THICKNESS': 'log',
    'He_CORE_MASS': 'log',

}

LABEL_DICT = {
    'MASS': r'Mass [M$_{\odot}$]',
    'BURNING_SHELL_THICKNESS': r'Relative mass included in the burning shell [$M_{\mathrm{shell}}/M_{\mathrm{star}}$]',
    'RADIUS': r'Photosphere radius [R$_{\odot}$]',
    'LUMINOSITY': r'Photosphere luminosity [L$_{\odot}$}]',
    'AGE': r'Model age [$yr$]',
    'He_CORE_MASS': r'Relative helium core mass [$M_{\mathrm{core}}/M_{\mathrm{star}}$]',
    'CONVECTIVE_CORE_MASS': r'Relative convective core mass [$M_{\mathrm{core}}/M_{\mathrm{star}}$]',
    'CONVECTIVE_CORE_RADIUS': r'Relative convective core radius [$R_{\mathrm{core}}/R_{\mathrm{star}}$]',
    'CONVECTIVE_CORE_MASS_OVERSHOOT': r'Relative convective core mass including overshooting mixing [$M_{\mathrm{core}}/M_{\mathrm{star}}$]',
    'CONVECTIVE_CORE_RADIUS_OVERSHOOT': r'Relative convective core radius including overshooting mixing [$R_{\mathrm{core}}/R_{\mathrm{star}}$]',
    'CONVECTIVE_ENVELOPE_MASS': r'Relative convective envelope mass [$M_{\mathrm{env}}/M_{\mathrm{star}}$]',
    'CONVECTIVE_ENVELOPE_RADIUS': r'Relative convective envelope radius [$R_{\mathrm{env}}/R_{\mathrm{star}}$]',
    'CONVECTIVE_ENVELOPE_MASS_SIMPLIFIED': r'Relative convective envelope mass from simplified Ledoux [$M_{\mathrm{env}}/M_{\mathrm{star}}$]',
    'CONVECTIVE_ENVELOPE_RADIUS_SIMPLIFIED': r'Relative convective envelope radius from simplified Ledoux [$R_{\mathrm{env}}/R_{\mathrm{star}}$]',
    'K2': r'apsidal constant',
    'TIDAL_E2': r'tidal E2 from Zahn',
    'TIDAL_E_FOR_LAMBDA': r"tidal E for Zahn's lambda",
    'MOMENT_OF_INERTIA_FACTOR': 'beta^2 from Claret AA 541, A113 (2012)= I/(MR^2)',
    'TIMESCALE_KELVIN_HELMHOLTZ': r'Kelvin-Helmholtz timescale [$yr$]',
    'TIMESCALE_DYNAMICAL': r'Dynamical timescale [$yr$]',
    'TIMESCALE_NUCLEAR': r'Nuclear timescale [$yr$]',
    'MEAN_MOLECULAR_WEIGHT_CORE': r'mean molecular weight at central mesh point',
    'MEAN_MOLECULAR_WEIGHT_AVERAGE': r'mean molecular weight average through star',
    'FIRST_DERIVATIVE_BURNING_SHELL_THICKNESS': r'First derivative of relative mass included in the burning shell [$M_{\mathrm{shell}}/M_{\mathrm{star}}/yr$]',
    'SECOND_DERIVATIVE_BURNING_SHELL_THICKNESS': r'Second derivative of relative mass included in the burning shell [$M_{\mathrm{shell}}/M_{\mathrm{star}}/yr^{2}$]'
}


CONTOURLEVEL_DICT = {
    'AGE': [1e6, 1e7, 1e8, 1e9, 1e10]
}



def general_plot_routine(dataset_filename, output_pdfs_dir, combined_pdf_filename, show_plots=False):
    """
    Function to plot all the columns (non-structural) of a MINT dataset.

    Input:
        dataset_filename: path to the MINT dataset
    """

    #
    x_scale = 'log'
    y_scale = 'log'
    y_resolution = 1000

    # Make output dir if necessary:
    os.makedirs(output_pdfs_dir, exist_ok=True)

    # Get the dataframes and some info about the columns
    dataframe_dict, non_chebychev_column_names_dict  = readout_datafile_and_return_dataframes(dataset_filename)

    # Determine the columns we need to plot
    all_column_names = sorted(non_chebychev_column_names_dict.keys(), key=lambda x: x)
    xy_column_names = ['MASS', 'BURNING_SHELL_THICKNESS']
    plot_column_names = sorted(list(set(all_column_names)-set(xy_column_names)))

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_page_number = 0

    print("Plotting HG results for {}".format(dataset_filename))

    # Go over the columns
    for column_name in plot_column_names:
        output_name = os.path.join(output_pdfs_dir, '{}.pdf'.format(column_name))
        print("\tplotting {}".format(column_name))
        plot_with_unstructured_data(
            dataframe_dict,

            xy_column_names[0],
            xy_column_names[1],
            column_name,

            x_scale,
            y_scale,
            Z_SCALE_DICT[column_name],
            y_resolution=y_resolution,
            contourlevel_dict=CONTOURLEVEL_DICT,
            label_dict=LABEL_DICT,
            plot_settings={'runname': column_name, 'show_plot': show_plots, 'output_name': output_name}
        )
        merger, pdf_page_number = add_pdf_and_bookmark(merger, output_name, pdf_page_number, bookmark_text="{}".format(column_name))

    # wrap up the pdf
    merger.write(combined_pdf_filename)
    merger.close()
    print("\tFinished plotting HG results for {}".format(dataset_filename))
    print("\t wrote combined pdf to {}".format(combined_pdf_filename))
