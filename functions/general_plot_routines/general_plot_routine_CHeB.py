"""
General plot routine for the MS grid
"""

import os
import pandas as pd

from PyPDF2 import PdfFileMerger

from functions.functions import readout_datafile_and_return_dataframes, plot_data_2d, create_plotting_data, plot_with_unstructured_data
from functions.plot_utility_functions import add_pdf_and_bookmark


Z_SCALE_DICT = {
    'AGE': 'log',
    'CONVECTIVE_CORE_MASS': 'log',
    'CONVECTIVE_CORE_MASS_OVERSHOOT': 'log',
    'CONVECTIVE_CORE_RADIUS': 'log',
    'CONVECTIVE_CORE_RADIUS_OVERSHOOT': 'log',
    'CONVECTIVE_ENVELOPE_MASS': 'log',
    'CONVECTIVE_ENVELOPE_MASS_SIMPLIFIED': 'log',
    'CONVECTIVE_ENVELOPE_RADIUS': 'log',
    'CONVECTIVE_ENVELOPE_RADIUS_SIMPLIFIED': 'log',
    'FIRST_DERIVATIVE_CENTRAL_HYDROGEN': 'symlog',
    'K2': 'log',
    'LUMINOSITY': 'log',
    'MEAN_MOLECULAR_WEIGHT_AVERAGE': 'log',
    'MEAN_MOLECULAR_WEIGHT_CORE': 'log',
    'MOMENT_OF_INERTIA_FACTOR': 'log',
    'RADIUS': 'log',
    'SECOND_DERIVATIVE_CENTRAL_HYDROGEN': 'symlog',
    'TIDAL_E2': 'log',
    'TIDAL_E_FOR_LAMBDA': 'log',
    'TIMESCALE_DYNAMICAL': 'log',
    'TIMESCALE_KELVIN_HELMHOLTZ': 'log',
    'TIMESCALE_NUCLEAR': 'log',

    'FIRST_DERIVATIVE_BURNING_SHELL_THICKNESS': 'symlog',
    'SECOND_DERIVATIVE_BURNING_SHELL_THICKNESS': 'symlog',
    'BURNING_SHELL_THICKNESS': 'log',
    'He_CORE_MASS': 'log',

    'MASS': 'log',
    'CENTRAL_HELIUM': 'log',
    'HELIUM_CORE_MASS': 'log',
    'HELIUM_CORE_RADIUS': 'log',

    'FIRST_DERIVATIVE_CENTRAL_HYDROGEN': 'symlog',
    'SECOND_DERIVATIVE_CENTRAL_HYDROGEN': 'symlog',
    'FIRST_DERIVATIVE_CENTRAL_CARBON': 'symlog',
    'SECOND_DERIVATIVE_CENTRAL_CARBON': 'symlog',
    'FIRST_DERIVATIVE_CENTRAL_OXYGEN': 'symlog',
    'SECOND_DERIVATIVE_CENTRAL_OXYGEN': 'symlog',
    'FIRST_DERIVATIVE_HELIUM_CORE_MASS': 'symlog',
}

LABEL_DICT = {
    'MASS': r'Mass [M$_{\odot}$]',
    'CENTRAL_HELIUM': r'central helium abundance XHe',
    'HELIUM_CORE_MASS': r'fractional he core mass',
    'RADIUS': r'Photosphere radius [R$_{\odot}$]',
    'LUMINOSITY': r'Photosphere luminosity [L$_{\odot}$}]',
    'AGE': r'Model age [$yr$]',
    'HELIUM_CORE_RADIUS': 'relative core radius (Rcore/Rstar)',
    'CONVECTIVE_ENVELOPE_MASS': r'Relative convective envelope mass [$M_{\mathrm{env}}/M_{\mathrm{star}}$]',
    'CONVECTIVE_ENVELOPE_RADIUS': r'Relative convective envelope radius [$R_{\mathrm{env}}/R_{\mathrm{star}}$]',
    'TIMESCALE_KELVIN_HELMHOLTZ': r'Kelvin-Helmholtz timescale [$yr$]',
    'TIMESCALE_DYNAMICAL': r'Dynamical timescale [$yr$]',
    'TIMESCALE_NUCLEAR': r'Nuclear timescale [$yr$]',
    'MEAN_MOLECULAR_WEIGHT_CORE': r'mean molecular weight at central mesh point',
    'MEAN_MOLECULAR_WEIGHT_AVERAGE': r'mean molecular weight average through star',
    'FIRST_DERIVATIVE_CENTRAL_HYDROGEN': r'dYcore/dt [$yr^{-1}$]',
    'SECOND_DERIVATIVE_CENTRAL_HYDROGEN': r'd2Ycore/dt2 [$yr^{-2}$]',
    'FIRST_DERIVATIVE_CENTRAL_CARBON': r'dXCcore/dt [$yr^{-1}$]',
    'SECOND_DERIVATIVE_CENTRAL_CARBON': r'd2XCcore/dt2 [$yr^{-2}$]',
    'FIRST_DERIVATIVE_CENTRAL_OXYGEN': r'dXOcore/dt [$yr^{-1}$]',
    'SECOND_DERIVATIVE_CENTRAL_OXYGEN': r'd2XOcore/dt2 [$yr^{-2}$]',
    'FIRST_DERIVATIVE_HELIUM_CORE_MASS': r'dMc/dt [$M_{\odot} yr^{-1}$]',
    'K2': r'apsidal constant',
    'TIDAL_E2': r'tidal E2 from Zahn',
    'TIDAL_E_FOR_LAMBDA': r"tidal E for Zahn's lambda",
    'MOMENT_OF_INERTIA_FACTOR': 'beta^2 from Claret AA 541, A113 (2012)= I/(MR^2)',
}

CONTOURLEVEL_DICT = {
    'AGE': [1e6, 1e7, 1e8, 1e9, 1e10]
}

def general_plot_routine(dataset_filename, output_pdfs_dir, combined_pdf_filename, show_plots=False):
    """
    Function to plot all the columns (non-structural) of a MINT dataset.

    Input:
        dataset_filename: path to the MINT dataset
    """

    #
    x_scale = 'linear'
    y_scale = 'linear'
    y_resolution = 1000

    # Make output dir if necessary:
    os.makedirs(output_pdfs_dir, exist_ok=True)

    # Get the dataframes and some info about the columns
    dataframe_dict, non_chebychev_column_names_dict  = readout_datafile_and_return_dataframes(dataset_filename)

    # Create a combined dataframe
    combined_df = pd.DataFrame()
    for df_key, df in dataframe_dict.items():
        combined_df = pd.concat([combined_df, df], ignore_index=True)
    combined_df['probability'] = 1

    # #
    # plot_columns = ['MASS', 'CENTRAL_HELIUM', 'HELIUM_CORE_MASS']
    # scales = ['log',  'linear', 'linear']

    # #
    # generate_triangle_plot(
    #     combined_df,
    #     scales=scales,
    #     plot_columns=plot_columns,
    #     plot_settings={'show_plot': True, 'max_diff_threshold': 4},
    #     probability_column='probability'
    # )

    # Determine the columns we need to plot
    all_column_names = sorted(non_chebychev_column_names_dict.keys(), key=lambda x: x)
    xy_column_names = ['HELIUM_CORE_MASS', 'CENTRAL_HELIUM']
    plot_column_names = sorted(list(set(all_column_names)-set(xy_column_names)))

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_page_number = 0

    # We need to provide a query to narrow down the models

    query = 'MASS == {}'.format(combined_df['MASS'].unique()[0]) 
    # query = '0.5 <= CENTRAL_HELIUM <= 0.6'
    print("Plotting HG results for {} with query {}".format(dataset_filename, query))

    # Go over the columns
    for column_name in plot_column_names:
        output_name = os.path.join(output_pdfs_dir, '{}.pdf'.format(column_name))

        if column_name == 'FIRST_DERIVATIVE_HELIUM_CORE_MASS':
            continue


        print("\tplotting {}".format(column_name))
        plot_with_unstructured_data(
            dataframe_dict,

            xy_column_names[0],
            xy_column_names[1],
            column_name,

            x_scale,
            y_scale,
            Z_SCALE_DICT[column_name],

            y_resolution=y_resolution,
            contourlevel_dict=CONTOURLEVEL_DICT,
            label_dict=LABEL_DICT,
            plot_settings={'runname': column_name, 'show_plot': show_plots, 'output_name': output_name},
            query=query
        )
        merger, pdf_page_number = add_pdf_and_bookmark(merger, output_name, pdf_page_number, bookmark_text="{}".format(column_name))


        # ####
        # # Create triangle plot
        # combined_df = combined_df.query(query)

        # #
        # plot_columns = ['MASS', 'CENTRAL_HELIUM', 'HELIUM_CORE_MASS', column_name]
        # scales = ['log',  'linear', 'linear', Z_SCALE_DICT[column_name]]

        # #
        # generate_triangle_plot(
        #     combined_df,
        #     scales=scales,
        #     plot_columns=plot_columns,
        #     plot_settings={'runname': column_name, 'show_plot': show_plots, 'output_name': output_name},
        #     probability_column='probability'
        # )
        # merger, pdf_page_number = add_pdf_and_bookmark(merger, output_name, pdf_page_number, bookmark_text="{}".format(column_name))

    # wrap up the pdf
    merger.write(combined_pdf_filename)
    merger.close()
    print("\tFinished plotting HG results for {}".format(dataset_filename))
    print("\t wrote combined pdf to {}".format(os.path.abspath(combined_pdf_filename)))
